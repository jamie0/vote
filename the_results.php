<?php
require "init.inc.php";

require "header.php";

$_poll = new Poll($_GET["poll_id"]);

foreach($_poll->categories as $_cat):
?>
<div class="poll-category">
	<h2><?php echo $_cat->name; ?></h2>
<?php
	$_res = Database::Query("SELECT O.`name` AS `opt_name`, COUNT(*) AS `votes` FROM `votes` V LEFT JOIN `poll_options` O on V.`option` = O.`id` WHERE V.`poll_id`=? AND V.`category_id` = ? AND V.`verified` = '1' GROUP BY `option` ORDER BY `votes` DESC;", $_GET["poll_id"], $_cat->id)->fetchAll();
	$_first = true;
	foreach($_res as $_r):
?>
		<p<?php if($_first) echo " class='winner'"; $_first = false; ?>><b><?php echo $_r["opt_name"]; ?>:</b> <?php echo $_r["votes"]; ?></p>
<?php
	endforeach;
?>
</div>
<?php
endforeach;
require "footer.php";
?>