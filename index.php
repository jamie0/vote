<?php
require "init.inc.php";

$_poll_id = (int) @$_GET["id"];
$_poll = new Poll($_poll_id);

$_step = ((int) @$_GET["step"]) + 1;

if($_step < 0 || $_step > ($_total_steps = sizeof($_poll->categories)))
	Security::Terminate();

if(!empty($_POST)){
	if(!Security::CurrentUser($_poll_id) || @$_POST["hc"] != Security::CurrentUser($_poll_id))
		throw new Exception("You ran out of time, sorry!");

	if($_POST["email"] != null){
		if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))
			throw new Exception("Invalid email!");
		if(!$_poll->canVote($_SESSION["email"])){
			header("Location: done.php");
			exit;
		}
		$_SESSION["email"] = $_POST["email"];
		header("Refresh: 0");
		exit;
	}

	$_category_id = (int) $_POST["bmac"];
	$_answer = $_POST["category-" . $_category_id . "-value"][0];

	// Is this a good answer?
	if(($_current_category = $_poll->categories[$_category_id]) == null)
		throw new Exception("I didn't recognise that poll question!");
	if($_current_category->options[$_answer] == null)
		throw new Exception("That wasn't a valid answer...");

	$_SESSION["answers" . $_poll_id][$_category_id] = $_answer;

	if($_step == $_total_steps){
		if($_total_steps - sizeof($_SESSION["answers" . $_poll_id]) != 0)
			throw new Exception("You missed a question.");

		$_poll->addAnswer($_SESSION["email"], $_SESSION["answers" . $_poll_id]);
		session_destroy();
		header("Location: done.php");
		exit;
	}

	header("Location: ?id=" . $_poll_id . "&step=" . $_step);
	exit;
}



if(!Security::CurrentUser($_poll_id)){
	if($_step > 1) throw new Exception("You ran out of time, sorry!");
	Security::CreateUser($_poll_id);
}
$_current_category = $_poll->categories[$_step];

require "header.php";

if(!isset($_SESSION["email"])):
?>
<div class="poll-category">
	<form action="" method="post">
		<input type="hidden" name="hc" value="<?php echo Security::CurrentUser($_poll_id); ?>" />
		<h2>Email</h2>
		<span>To stop fraudulent voting, we require users to provide us with an email. Don't worry, we won't use it for anything other than verifying you're a real person.</span>
		<input type="email" name="email" value="<?php echo htmlentities(@$_SESSION["email"]); ?>" />
		<input type="submit" value="Go" />
	</form>
</div>
<?php else: ?>
<div class="poll-category">
	<form action="" method="post">
		<input type="hidden" name="bmac" value="<?php echo $_current_category->id; ?>" />
		<input type="hidden" name="hc" value="<?php echo Security::CurrentUser($_poll_id); ?>" />
		<h2><?php echo $_current_category->name; ?></h2>
		<span><?php echo $_current_category->description; ?></span>
<?php if($_current_category->type != PollCategory::$USER):
	foreach($_current_category->options as $_option): ?>
		<div class="poll-option">
			<input type="<?php echo "radio"; // BROKEN! $_current_category->type == PollCategory::$MULTI ? "checkbox": "radio";?>" name="category-<?php echo $_current_category->id; ?>-value[]" id="opt-<?php echo $_option->id; ?>" value="<?php echo $_option->id; ?>"  /><label for="opt-<?php echo $_option->id; ?>"><b><?php echo $_option->name; ?></b><br />
			<p><?php echo $_option->description; ?></p></label>
		</div>
<?php endforeach;
	else: die("The admin screwed up the poll"); ?>

		<input type="text" name="category-<?php echo $_current_category->id; ?>-value" value="" /><br >
<?php endif; ?>
		<input type="submit" value="Next" />
		<p><i><?php echo $_step; ?> of <?php echo $_total_steps; ?></i></p>
	</form>
</div>
<?php
endif;
//require "footer.php";
?>
