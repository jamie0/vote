<?php
require "init.inc.php";

// todo: auth
$_poll = new Poll(1);
?>
<html>
	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Arial, Verdana, sans-serif;
			margin: 0;
			padding: 0;
		}
		html {
			height: 100%;
		}
		body {
			min-height: 100%;
		}
		#sidebar {
			width: 300px;
			background: #33393E;
			height: 100%;
			overflow-x: hidden;
			position: relative;
			overflow-y: auto;
		}
		#sidebar input[type=text], textarea	 {
			background: #F9F9F9;
			border: none;
			color: #000;
			padding: 4px 10px;
			border-radius: 4px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			resize: vertical;
		}
		#sidebar input[type=text]:first-of-type, #sidebar textarea {
			margin: 7px auto;
			display: block;
			width: 260px;
			font-size: 13pt;
		}
		#sidebar textarea {
			font-size: 10pt;
		}
		.header {
			padding: 3px 20px;
			background: rgba(255, 255, 255, 0.1);
			margin-bottom: 20px;
		}
		.header input[type=text] {
			background: none !important;
			color: #FFF !important;
			display: inline !important;
		}
		.header button {
			border: 2px solid #999;
			color: #EEE;
			font-size: 10pt;
			background: none;
			border-radius: 15px;
			outline: none !important;
			padding: 4px 7px;
			position: relative;
			top: -1px;
		}
		.header button:hover {
			border-color: #CCC;
			color: #FFF;
			background: rgba(255, 255, 255, 0.05);
		}
		.header button + input[type=text] {
			width: 200px !important;
			margin-left: 3px !important;
		}
		.header input[type=text]:hover {
			background: rgba(255, 255, 255, 0.1) !important;
		}
		.header input[type=text]:focus {
			color: #000 !important;
			background: #F9F9F9 !important;
		}
		fieldset {
			border: none;
		}
		ul {
			width: 260px;
			margin: 0 auto;
			list-style-type: none;
		}
		ul a {
			color: #DDE;
			display: block;
			border-bottom: 1px solid #556;
			padding: 10px 10px 10px 20px;
			text-decoration: none;
			font-size: 11pt;
		}
		ul a:hover {
			background: rgba(255, 255, 255, 0.1);
		}
		ul a.gt:after {
			content: "\203A";
			font-weight: bold;
			float: right;
		}
		ul a.new:before {
			content: "+";
			font-weight: bold;
			color: #FFF;
			display: inline-block;
			margin-left: -14px;
			padding-right: 5px;
			position: relative;
		}
		.view {
			display: block;
			transition: left 0.2s, opacity 0.1s ease-out 0.1s;
			position: absolute;
			left: -300px;
			width: 300px;
			opacity: 1;
		}
		.view.right {
			left: 300px;
		}
		.view.visible {
			left: 0px;
			opacity: 1;
			display: block !important;
		}
		a input[type=text] {
			margin: 0 !important;
			display: inline-block !important;
			width: 220px !important;
			background: none !important;
			color: #FFF !important;
			font-size: 10pt !important;
		}
		a input[type=text]:focus {
			background: #F9F9F9 !important;
			color: #000 !important;
		}
		a.yy /*lmao*/ {
			padding: 4px 10px;
		}
	</style>
	<script type="text/javascript">
		function show(thing){
			var elems = document.querySelectorAll("div.view");
			for(var i in elems){
				if(!elems[i].classList) continue;
				if(elems[i].id == thing)
					elems[i].classList.add("visible")
				else
					elems[i].classList.remove("visible")
			}
		}
		function newCat(){
			
		}
		window.addEventListener("load", function(){ show("view-home"); })
	</script>
<body>
	<form action="" method="post">
		<div id="sidebar">
			<div class="view" id="view-home">
				<div class="header">
					<input type="text" name="title" id="title" value="<?php echo htmlentities($_poll->name); ?>" />
				</div>
				<textarea name="desc" id="desc">Sample Description</textarea>
				<ul>
<?php foreach($_poll->categories as $_cat): ?>
					<li><a href="#" onclick="show('view-cat<?php echo $_cat->id; ?>')" class="gt"><?php echo htmlentities($_cat->name); ?></a></li>
<?php endforeach; ?>
					<li><a href="#" class="new" id="new-cat" onclick="newCat())">New Category</a></li>
				</ul>
			</div>
<?php foreach($_poll->categories as $_cat): ?>
			<div class="view right" id="view-cat<?php echo $_cat->id; ?>">
				<div class="header">
					<button onclick="show('view-home'); return false;">Back</button><input type="text" name="title<?php echo $_cat->id; ?>" id="title<?php echo $_cat->id; ?>" value="<?php echo htmlentities($_cat->name); ?>" placeholder="Title" />
				</div>
				<textarea name="desc<?php echo $_cat->id; ?>" id="desc<?php echo $_cat->id; ?>" placeholder="Description"><?php echo htmlentities($_cat->description); ?></textarea><br />
				<ul>
<?php foreach($_cat->options as $_opt): ?>
					<li><a href="#" onclick="show('view-<?php echo $_opt->id; ?>'); return false;" class="gt"><?php echo htmlentities($_opt->name); ?></a></li>
<?php endforeach; ?>
					<li><a href="#" class="new">New Option</a></li>
				</ul>
			</div>
<?php foreach($_cat->options as $_opt): ?>
			<div class="view right" id="view-opt1">
				<div class="header">
					<button onclick="show('view-cat1'); return false;">Back</button><input type="text" name="title" id="title" value="Botnet Swatnet" />
				</div>
				<input type="text" name="url" value="" style="font-size: 10pt" placeholder="Image Link"><br />
				<ul>
					<li><a href="#">Delete</a></li>
				</ul>
			</div>
<?php endforeach; ?>
<?php endforeach; ?>
		</div>
	</form>
</body>
