<?php
require "init.inc.php";

if(isset($_GET["hc"])){
	Database::Query("UPDATE `votes` SET verified=1 WHERE nonce=?;", $_GET["hc"]);
}
require "header.php";
?>
<div class="poll-category">
	<h2>Thanks!</h2>
	<span>Your vote has been confirmed!</span><br /><br />
</div>
<?php
require "footer.php";
?>