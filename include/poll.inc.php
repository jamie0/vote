<?php

class Poll {
	public function __construct($_data){
		if(is_numeric($_data))
			$_data = Database::Query("SELECT * FROM `polls` WHERE id=?;", $_data)->fetch();

		if($_data === false)
			throw new Exception("Poll doesn't exist");

		$this->id = $_data["id"];
		$this->name = $_data["name"];
		$this->description = $_data["description"];
		$this->categories = array();
		$_categories = Database::Query("SELECT * FROM `poll_categories` WHERE poll_id=?;", $_data["id"])->fetchAll();
		foreach($_categories as $_cat){
			$this->categories[$_cat["id"]] = new PollCategory($_cat);
		}


		$this->options = array();
		$_options = Database::Query("SELECT * FROM `poll_options` WHERE poll_id=?;", $_data["id"])->fetchAll();
		foreach($_options as $_opt){
			$this->categories[$_opt["category_id"]]->addOption($this->options[] = new PollOption($_opt));
		}
	}
	public function addAnswer($_email, $_answers){
		foreach($_answers as $_category => $_answer){
			Database::Query("INSERT INTO `votes` (ip, email, poll_id, category_id, option, nonce) VALUES (?, ?, ?, ?, ?, ?);",
				$_SERVER["REMOTE_ADDR"], $_email, $this->id, $_category, $_answer, Security::CurrentUser($this->id));
		}
		mail($_email, "Confirm your vote", "Hi!\nTo confirm your vote on " . $this->name . ", please click the following link:\n\n" . Config::$_url . "confirm.php?hc=" . Security::CurrentUser($this->id) . "\n\nKind Regards,\n" . Config::$_email_sig, "From: " . Config::$_email_from);
		return true;
	}
	public function canVote($_email){
		return Database::Query("SELECT * FROM `votes` WHERE (email=? OR ip=?) AND poll_id=?;", $_email, $_SERVER["REMOTE_ADDR"], $this->id)->rowCount() == 0;
	}
}

class PollCategory {
	// Single/Multiple option, and user input enum
	static $SINGLE = 0;
	static $MULTI = 1;
	static $USER = 2;
	public $options = array();
	public function __construct($_struct){
		$this->id = $_struct["id"];
		$this->name = $_struct["name"];
		$this->description = $_struct["description"];
		$this->type = $_struct["type"];
	}
	public function addOption($_option){
		$this->options[$_option->id] = $_option;
	}
}

class PollOption {
	public function __construct($_data){
		$this->id = $_data["id"];
		$this->name = $_data["name"];
		$this->description = $_data["description"];
	}
}

?>