<?php
// No idea what the  point of this really is, but yeah, I got bored.
class Storage {
	static function Search($_table, $_where, $_mutable = false){
		$_query = "SELECT * FROM `$_table`";
		$_opts = array();
		if($_where != null){
			foreach($_where as $_key => $_opt){
				if(is_int($_key)) $_query .= $_opt . " AND";
				if(!preg_match("/[a-zA-Z0-9_]/", $_key)) trigger_error("Invalid column name supplied", E_ERROR);	
				$_opts[] = $_opt;
				$_query .= " `" . $_key . "`=? AND";
			}
			$_query = substr($_query, 0, -4); // strip AND from the end
		}
		$_result = Database::$pdo->prepare($_query . ";");
		$_result->execute(array($_opts));
		$_data = array_map($_result->fetchAll(), function(){
			$_mutable ? new MutableStorageElement($_data, $_table, Database::$pdo) :
				new StorageElement($_data, $_table, Database::$pdo);
		});
		return $_data;
	}
}
class StorageElement {
	function __construct($_data, $_table, $_pdo = null){
		$this->data = $_data;
		// build a "structure" from the associative array returned
		$this->structure = array_map($_data, function(){ return true; });
		$this->table = $_table;
		$this->pdo = $_pdo;
	}
	function __get($_name){
		return $this->data[$_name];
	}
	function __set($_name, $_value){
		trigger_error("Tried to mutate immutable object", E_RECOVERABLE_ERROR);
	}
}
class MutableStorageElement extends StorageElement {
	function __set($_name, $_value){
		if(!$this->structure[$_name])
			return trigger_error("Tried to mutate non-existent storage object", E_RECOVERABLE_ERROR);
		try {
			$this->pdo->query("UPDATE `{$this->$_table}` SET `$_name` = ?;")->execute(array($_value));
		} catch (Exception $e){
			trigger_error("Database error: " . $e->getMessage(), E_RECOVERABLE_ERROR);
		}
	}
}