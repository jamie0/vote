<?php
session_name("_poll_user");
session_start();
class Security {
	public static function CreateUser($_poll_id){
		$_SESSION["id_" . $_poll_id] = array(
			// Generate the nonce
			substr(str_replace(str_split("+/"), "", base64_encode(pack("H*", hash("sha512", mt_rand() . time() . var_export($_SERVER, true))))), 0, 16),
			// And the expiry time
			strtotime("+ 6 hours")
		);
	}
	public static function CurrentUser($_poll_id){
		if(isset($_SESSION["id_" . $_poll_id])){
			if($_SESSION["id_" . $_poll_id][1] < time())
				return false;
			return $_SESSION["id_" . $_poll_id][0];
		}
		return false;
	}
}
