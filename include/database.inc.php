<?php
class Database {
	public static $isConnected = false;
	public static $pdo;
	public static function Connect(){
		if(self::$isConnected) return;
		self::$pdo = new PDO(Config::$_database["driver"], Config::$_database["username"], Config::$_database["password"]);
		self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		self::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	}
	public static function Query($_query, $_params){
		$_query = self::$pdo->prepare($_query);
		$_query->setFetchMode(PDO::FETCH_ASSOC);
		$_params = array_slice(func_get_args(), 1);
		$_query->execute($_params);
		return $_query; // run $_query->fetch();
	}
	public static function InsertID(){
		return self::$pdo->lastInsertId();
	}
	public static function Result($_query, $_params = array()){
		$_query = self::Query($_query, $_params);
		return $_query->fetchColumn(); // for just one column
	}
	public static function Select($_table, $_params = array()){
		$_values = array();
		if($_params != null){
			$_where = "WHERE ";
			foreach($_params as $_column => $_value){
				$_where .= "`$_column`=? AND";
				$_values[] = $_value;
			}
			$_where = rtrim($_where, " AND");
		}
		$_query = @self::$pdo->prepare("SELECT * FROM `$_table` $_where;");
		$_query->execute($_values);
		$_query->setFetchMode(PDO::FETCH_ASSOC);
		return $_query;
	}
	public static function Insert($_table, $_columns){
		if(sizeof($_columns) == 0) throw new Exception("No parameters provided for insert");
		$_column = "`" . implode("`, `", $_columns) . "`";
		foreach($_columns as $_col=>$_val){
			if(is_object($_col)){ // we need to 
				if($_col->function != null)
					$_value_placeholders .= $_col->function . ", "; // add "function" support, like CURRENT_TIMESTAMP
				continue;
			}
			$_value_placeholders .= "?, ";
			$_values[] = $_val;
		}
		$_value_placeholders = rtrim($_value_placeholders, ", ");
		$_query = self::$pdo->prepare("INSERT INTO `$_table` ($_column) VALUES ($_value_placeholder);");
		$_query->execute($_values);
		return $_query;
	}
	public static function InsertUpdate($_table, $_update, $_where){
		if(sizeof($_columns) == 0) throw new Exception("No parameters provided for insert");
		$_column = "`" . implode("`, `", $_columns) . "`";
		$_value_placeholders = str_repeat("?, ", sizeof($_columns));
		$_value_placeholders = rtrim($_value_placeholders, ", ");
		foreach($_columns as $_val)
			$_values[] = $_val;
		$_query = self::$pdo->prepare("INSERT INTO `$_table` ($_column) VALUES ($_value_placeholder);");
		$_query->execute($_values);
		return $_query;
	}
}
Database::Connect();