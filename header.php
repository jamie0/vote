<!DOCTYPE html>
<html>
<head>
	<title>Vote</title>
	<meta charset="UTF-8" />
	<style type="text/css">
		@import url(http://fonts.googleapis.com/css?family=Skranji);
		* {
			margin: 0;
			padding: 0;
		}
		body {
			background: #EEE;
		}
		.poll-category {
			background: #FFF;
			padding: 30px 30px 15px 30px;
			font-family: "Helvetica Neue", "Arial", "Verdana", sans-serif;
			font-size: 10pt;
			width: 500px;
			margin: 20px auto;
			text-align: right;
		}
		.poll-category span {
			display: block;
			text-align: center;
		}
		.poll-category h2 {
			text-align: center;
		}
		/* Title / Option font */
		.poll-category h2, .poll-option b {
			font-family: "Skranji", cursive;
			font-weight: normal;
		}
		.poll-option {
			float: left;
			text-align: center;
			width: 49%;
		}

		.poll-option b {
			padding: 3px 10px;
			font-size: 14pt;
		}
		.poll-option input {
			position: relative;
			top: 1px;
		}
		input[type=email] {
			display: block;
			margin: 5px auto;
			font-size: 10pt;
			padding: 5px 10px;
			width: 250px;
		}
		.winner {
			color: #050;
		}
	</style>
</head>
<body>