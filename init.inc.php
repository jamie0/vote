<?php
require_once "config.inc.php";
require_once "include/database.inc.php";
require_once "include/storage.inc.php";
require_once "include/security.inc.php";
require_once "include/poll.inc.php";

function check_cidr($_ip, $_cidr){
	list($_start, $_subnet) = split("/", $_cidr);
	$_start = ip2long($_start);
	$_ip = ip2long($_ip);
	$_subnet = ~((1 << (32 - $_subnet)) - 1);
	return ($_ip & $_subnet) == $_start;
}
call_user_func(function(){
	$_ip4 = explode(",", "199.27.128.0/21,173.245.48.0/20,103.21.244.0/22,103.22.200.0/22,103.31.4.0/22,141.101.64.0/18,108.162.192.0/18,190.93.240.0/20,188.114.96.0/20,197.234.240.0/22,198.41.128.0/17,162.158.0.0/15,104.16.0.0/12,127.0.0.0/8");
	$_cloudflare = false;
	foreach($_ip4 as $_ip)
		if(check_cidr(@$_SERVER["REMOTE_ADDR"], $_ip)) $_cloudflare = true;
	
	if($_cloudflare && @$_SERVER["HTTP_X_FORWARDED_FOR"] != ""){
		$_forwarded = explode(",", $_SERVER["HTTP_X_FORWARDED_FOR"]);	
		$_SERVER["REMOTE_ADDR"] = array_shift($_forwarded);
		$_SERVER["HTTP_X_FORWARDED_FOR"] = implode(",", $_forwarded);
	}
});
