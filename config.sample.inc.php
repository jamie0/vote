<?php
class Config {
	public static $_database = array(
		"driver" => "mysql:host=localhost:3306;dbname=vote",
		"username" => "root",
		"password" => ""
	);
	public static $_url = "http://www.example.com/vote/";
	public static $_email_sig = "The Website Team";
	public static $_email_from = "The Website <noreply@example.com>";
}